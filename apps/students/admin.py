from django.contrib import admin
from apps.students.models import Student, StudentInfo, StudentAddress

admin.site.register(StudentInfo)
admin.site.register(StudentAddress)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'modified_date')
    list_display_links = ('first_name', 'last_name')
    search_fields = ('first_name', 'last_name')
    readonly_fields = ('birth_date',)
