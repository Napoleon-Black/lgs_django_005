from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, Form

from apps.students.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'phone', 'birth_date')


class SearchStudentForm(Form):
    search = forms.CharField(max_length=50, required=True)
