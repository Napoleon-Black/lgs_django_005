from django.urls import path

from apps.students.views import add_student, Students

urlpatterns = [
    path('', Students.as_view(), name='students'),
    path('add-student/', add_student, name='add_student')
]
