from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import FormView

from apps.students.forms import StudentForm, SearchStudentForm
from apps.students.models import Student


# @method_decorator(user_passes_test(lambda user: user.is_superuser), name='post')
# @method_decorator(login_required, name='dispatch')
class Students(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        form = SearchStudentForm()
        students = Student.objects.all()

        return render(
            request,
            'students/students.html',
            {'students': students, 'form': form}
        )

    def post(self, request, *args, **kwargs):
        form = SearchStudentForm(request.POST)

        if form.is_valid():
            search = form.cleaned_data.get('search')
            students = Student.objects.filter(
                Q(first_name__icontains=search)
                | Q(last_name__icontains=search)
            )
        else:
            students = Student.objects.all()

        return render(
            request,
            'students/students.html',
            {'students': students, 'form': form}
        )


# @login_required
# @staff_member_required
@user_passes_test(lambda user: user.is_superuser)
def add_student(request):
    if request.method == 'GET':
        form = StudentForm()

    if request.method == 'POST':
        form = StudentForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('students')

    return render(
        request,
        'students/add_student.html',
        {'form': form}
    )


class CreateUser(FormView):
    template_name = 'registration/register.html'
    form_class = UserCreationForm
    success_url = '/'


def create_user(request):
    if request.method == 'GET':
        form = UserCreationForm()

    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('students')

    return render(
        request,
        'registration/register.html',
        {'form': form}
    )