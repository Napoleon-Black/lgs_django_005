from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver

from apps.students.models import Student


@receiver(pre_save, sender=Student)
def test_pre_save(instance, **kwargs):
    print(instance, kwargs)


@receiver(post_delete, sender=Student)
def test_post_delete(instance, **kwargs):
    print(instance, kwargs)
