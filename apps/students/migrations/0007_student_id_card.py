# Generated by Django 2.2.7 on 2019-11-28 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0006_auto_20191128_1731'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='id_card',
            field=models.CharField(default='0000000000', max_length=10),
            preserve_default=False,
        ),
    ]
