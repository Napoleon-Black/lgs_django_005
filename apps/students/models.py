from django.db import models


class Student(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=15, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    modified_date = models.DateTimeField(auto_now=True)
    id_card = models.CharField(max_length=10)

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class StudentInfo(models.Model):
    student = models.OneToOneField(
        Student, on_delete=models.CASCADE
    )
    gender = models.CharField(
        max_length=6,
        choices=(
            ('male', 'Male'),
            ('female', 'Female'),
        ),
        null=True,
        blank=True
    )

    def __str__(self):
        return f'{self.student.first_name} {self.student.last_name}'


class StudentAddress(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE
    )
    country = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.student.full_name}'
