from django.db import models

from apps.students.models import Student


class Course(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=10)
    last_modified = models.DateTimeField(auto_now=True)


class CourseStudent(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE
    )
    course = models.ForeignKey(
        Course, on_delete=models.CASCADE
    )
    note = models.TextField()
