from django.urls import path

from apps.courses.views import Courses

urlpatterns = [
    path('courses/', Courses.as_view(), name='courses'),
]
