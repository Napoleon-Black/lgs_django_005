from django.contrib import admin

from apps.courses.models import Course, CourseStudent

admin.site.register(Course)
admin.site.register(CourseStudent)
